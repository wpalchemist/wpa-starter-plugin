<?php
/*
Plugin Name: WPA Starter Plugin
Plugin URI: http://wpalchemists.com
Description: Basic setup to start a plugin
Version: 1
Author: Morgan Kay
Author URI: http://wpalchemists.com
Text Domain: wpasp
*/

/*  Copyright 2014 Morgan Kay (email : morgan@wpalchemists.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ------------------------------------------------------------------------
// REQUIRE MINIMUM VERSION OF WORDPRESS:                                               
// ------------------------------------------------------------------------


function wpasp_requires_wordpress_version() {
	global $wp_version;
	$plugin = plugin_basename( __FILE__ );
	$plugin_data = get_plugin_data( __FILE__, false );

	if ( version_compare($wp_version, "3.8", "<" ) ) {
		if( is_plugin_active($plugin) ) {
			deactivate_plugins( $plugin );
			wp_die( "'".$plugin_data['Name']."' requires WordPress 3.8 or higher, and has been deactivated! Please upgrade WordPress and try again.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
		}
	}
}
add_action( 'admin_init', 'wpasp_requires_wordpress_version' );

// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
register_activation_hook(__FILE__, 'wpasp_add_defaults');
register_uninstall_hook(__FILE__, 'wpasp_delete_plugin_options');
add_action('admin_init', 'wpasp_init' );
add_action('admin_menu', 'wpasp_add_options_page');
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'wpasp_plugin_action_links', 10, 2 );

// Require options stuff
require_once( plugin_dir_path( __FILE__ ) . 'options.php' );
// Require views
require_once( plugin_dir_path( __FILE__ ) . 'views.php' );


// Initialize language so it can be translated
function wpasp_language_init() {
  load_plugin_textdomain( 'wpasp', false, dirname(plugin_basename(__FILE__)) . 'languages' );
}
add_action('init', 'wpasp_language_init');


// ------------------------------------------------------------------------
// PLUGIN FUNCTIONALITY
// ------------------------------------------------------------------------


?>
